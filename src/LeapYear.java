import java.util.Scanner;

public class LeapYear {

    public static void main(String[] args) {

        int currentYear;
        Scanner yearScanner = new Scanner(System.in);

        System.out.println("What year is it?");
        currentYear = yearScanner.nextInt();
        if (currentYear % 4 == 0) {
            if (currentYear % 400 == 0) {
                System.out.println(currentYear + " is a leap year.");
            }
            else if (currentYear % 100 == 0) {
                System.out.println(currentYear + " is not a leap year.");
            }
        } else {
            System.out.println(currentYear + " is not a leap year.");
        }

    }

}
